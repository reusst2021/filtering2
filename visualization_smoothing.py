# -*- coding: utf-8 -*-
"""

Created on Mon Jun  7 14:23:37 2021

@author: jilli
"""

from scipy.io import loadmat
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
from scipy import signal

#loading a reading the data from the file

mat = loadmat('data1.mat')
time_data = mat['time']
X_data = mat['X']

#extracting displacement data
displ = X_data[:,1]

#extracting force data
force = X_data[:,2]

#creating sample size variable
s = 50
#scipy sampling time and displacement
displ_sample = signal.resample(displ,s)
time_sample = signal.resample(time_data,s)
force_sample = signal.resample(force,s)

#initializing velocity vector with zeros
velocity = np.zeros(time_sample.shape[0])

#creating the velocity values
for i in range(time_sample.shape[0]):
    dx = displ_sample[i]-displ_sample[i-1]
    dt = time_sample[i]-time_sample[i-1]
    velocity[i]=float(dx/dt)

#the first figure (separate plots)
plt.figure(0)

#plotting
plt.plot(velocity, force_sample, 'tab:green')
plt.title('Force vs Velocity')
plt.xlabel('Velocity')
plt.ylabel('Force(kips)') 


